package freeAquarium.chess

import java.io.Serializable

/*
  this class holds the status of the uci info string and it updates it on every update() call
  isWhite is needed, because the score from the engine is from the side the engine plays, means if black is better is has a positive value
*/
data class UciInfoState(var isWhite: Boolean = true, var depth: Short = 0, var score: Int = 0, var bestmove: String = "", var pv: String = "", var nps: Int = 0,
                        var hashfull: Int = 0, var tbhits: Long = 0, var time: Long = 0) : Serializable {
    /*
     first boolean is true if depth get greater, or first pv move changes
     second boolean if other values changed
     */
    fun update(info: String): Pair<Boolean, Boolean> {
        var stateChanged = false
        var frontendOnly = false

        if (info.startsWith("info ")
                && !info.contains("time -1")) { // berserk send this
            var m = depth_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thedepth = m.groups[1]!!.value.toShort()
                if (thedepth >= depth) {
                    depth = thedepth
                    stateChanged = true
                }
            }

            m = score_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thescore = m.groups[1]!!.value.toInt()
                val score_bw = if (isWhite) thescore else 0 - thescore
                if (score_bw != score) {
                    score = score_bw
                    frontendOnly = true    // state change?
                }
            }
            m = pv_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thepv = m.groups[1]!!.value
                val thebest = m.groups[2]!!.value
                if (thebest.isNotEmpty() && thebest != bestmove) {
                    stateChanged = true
                    bestmove = thebest
                }
                if (thepv.isNotEmpty() && thepv != pv) {
                    pv = thepv
                    frontendOnly = true
                }
            }
            m = nps_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thenps = m.groups[1]!!.value.toInt()
                if (thenps > 0) {
                    nps = thenps
                    frontendOnly = true
                }
            }
            m = hash_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thehash = m.groups[1]!!.value.toInt()
                if (thehash > 0) {
                    hashfull = thehash
                    frontendOnly = true
                }
            }
            m = tbhits_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thetb = m.groups[1]!!.value.toLong()
                if (thetb > 0) {
                    tbhits = thetb
                    frontendOnly = true
                }
            }
            m = time_regex.find(info)
            if (m != null && m.groups.isNotEmpty()) {
                val thetime = m.groups[1]!!.value.toLong()
                if (thetime > 0) {
                    time = thetime
                    frontendOnly = true
                }
            }
        }

        return Pair(stateChanged, frontendOnly)
    }

    companion object {
        private val depth_regex = Regex("depth (\\d+)")
        private val pv_regex = Regex(" pv ((\\w*) .*\$)")
        private val score_regex = Regex("score cp (-?\\d+)")
        private val nps_regex = Regex(" nps (\\d+)")
        private val hash_regex = Regex("hashfull (\\d+)")
        private val tbhits_regex = Regex(" tbhits (\\d+)")
        private val time_regex = Regex(" time (\\d+) ")
    }
}
