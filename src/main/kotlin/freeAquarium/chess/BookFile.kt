package freeAquarium.chess

import java.io.*
import java.text.DecimalFormat
import java.util.*

private val fmt_d = DecimalFormat("+0.00;-0.00")
fun toHumanEval(score:Int?, depth:Long?=0L): String {
    if (score==null) return ""
    val ret = fmt_d.format(score / 100.0)
    return if (depth==null || depth==0L) ret else "$ret|$depth"
}

typealias BookTreeMap = TreeMap<Long, ArrayList<BookMove>>

fun ArrayList<BookMove>.getWeightOrNull(move:Int):Int? {
    forEach {
        if (it.move==move) return it.weight
    }
    return null
}

data class BookMove(val move:Int, var weight:Int)

fun DataInput.readBookMove(): BookMove {
    val move = readShort().toInt()   // move has bits 0-14, so it cant be signed -> Short
    val weight = java.lang.Short.toUnsignedInt(readShort())
    readInt()  // skip learn
    return BookMove(move, weight)
}

fun DataOutput.writeBookMove(entry: BookMove) {
    writeShort(entry.move)
    writeShort(entry.weight)
    writeInt(0)     // skip learn
}

data class BookFile(private val pathName:String) {
    val book by lazy {
        val ret = BookTreeMap()     // the file should be sorted already
        if (pathName.isNotEmpty()) {
            try {
                val file = File(pathName)
                if (file.canRead()) {
                    DataInputStream(file.inputStream().buffered()).use {
                        while (it.available() != 0) {
                            val key = it.readLong()
                            val entry = ret.getOrPut(key, { ArrayList() })
                            entry.add(it.readBookMove())
                        }
                    }
                }
            }
            catch(e: IOException) {
                println("something went wrong on reading the book from file:$pathName \n$e")
            }
        }
        ret
    }

    private val randomAccessFile by lazy {
        try {
            RandomAccessFile(File(pathName), "r")
        }
        catch (e:IOException) {
            null
        }
    }

    fun getKey(key:Long): List<BookMove>? {
        if(randomAccessFile!=null) {
            val result = Collections.binarySearch(
                    bookRecordAdapter(randomAccessFile!!),
                    key,
                    { o1, o2 -> java.lang.Long.compareUnsigned(o1, o2) }
            )
            if(result>0) {
                val ret = mutableListOf<BookMove>()
                try {
                    randomAccessFile!!.seek((result * 16).toLong())
                    var bookKey = randomAccessFile!!.readLong()
                    while (bookKey == key) {
                        ret.add(randomAccessFile!!.readBookMove())
                        bookKey = randomAccessFile!!.readLong()
                    }
                }
                catch(ignore:EOFException) {}

                return ret
            }
        }
        return null
    }
}

private class bookRecordAdapter(val file:RandomAccessFile): kotlin.collections.AbstractList<Long>() {
    override val size: Int
        get() = (file.length() / 16).toInt()

    override fun get(index: Int): Long {
        return try {
            file.seek((index * 16).toLong())
            file.readLong()
        }
        catch (e:EOFException) {
            -1
        }
    }
}

fun writeBook(f:File, book: BookTreeMap) {
    if(f.canWrite()) {
        // dump it to stream
        try {
            val data = DataOutputStream(f.outputStream().buffered())
            writeToStream(book, data)
            data.close()
        } catch (e: IOException) {
            println("something went wrong on writing the book: $e")
        }
    }
    else {
        println("sorry, no write access to file: ${f.name}!")
    }
}

fun writeToStream(book: BookTreeMap, stream: DataOutputStream) {
    for (entry in book) {
        for (move in entry.value) {
            stream.writeLong(entry.key)
            stream.writeBookMove(move)
        }
    }
}